package web.controller;

import com.application.model.Company;
import com.application.model.User;
import com.application.web.controller.CompanyController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/** 
* CompanyController Tester. 
* 
* @author Albin
* @since <pre>juil. 20, 2021</pre> 
* @version 1.0 
*/ 
public class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void before() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new CompanyController()).build();
        Company.populate();
        User.populate();
    }

    @After
    public void after() throws Exception {
    }

    /**
    *
    * Method: distributeGiftCard(@PathVariable int id)
    *
    */
    @Test
    public void testDistributeGiftCard() throws Exception {
        this.mockMvc.perform(post("/company/distribute/gift/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("Wedoogift"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    /**
    *
    * Method: distributemealVourcher(@PathVariable int id)
    *
    */
    @Test
    public void testDistributemealVourcher() throws Exception {
        this.mockMvc.perform(post("/company/distribute/meal/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON)
                .content("Wedoofood"))
                .andDo(print())
                .andExpect(status().isCreated());
    }


} 
