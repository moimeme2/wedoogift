package com.application.model;

import com.application.exception.NegativeBalanceException;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Company Tester.
 *
 * @author Albin
 * @version 1.0
 * @since <pre>juil. 18, 2021</pre>
 */
public class CompanyTest {

    static final Logger LOGGER = Logger.getLogger(CompanyTest.class);

    private User user;
    private Company company;

    @Before
    public void before() throws Exception {
        company = new Company();
        company.setId(1);
        company.setName("Wedoogift");
        company.setBalance(1000);
        user = new User(1, 0, 1);
        company.setUserList(new ArrayList<>() {{
            add(user);
        }});
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: distributeGiftCard()
     */
    @Test
    public void testDistributeGiftCardsOK() throws Exception {
        company.distributeGiftCard(company.getId());
        Assert.assertEquals(100, user.getBalance(), 0);
    }

    /**
     * Method: distributeGiftCard()
     */
    @Test
    public void testDistributeMealVourcherOK() throws Exception {
        company.distributeMealVourcher(company.getId());
        LOGGER.info(user.getMealVourcherList().get(0).getCardValidity());
        Assert.assertEquals(user.getMealVourcherList().get(0).getCardValidity(), LocalDate.of(2022, 2, 28));
    }

    /**
     * Method: distributeGiftCard()
     */
    @Test(expected = NegativeBalanceException.class)
    public void testDistributeGiftCardsKOWhenAmountSuperiorToBalance() throws Exception {
        company.setBalance(99.999);
        company.distributeGiftCard(company.getId());
    }



} 
