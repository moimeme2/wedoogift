package com.application.model;

import com.application.model.gift.Advantage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Distribution {

    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(Distribution.class);

    private static final AtomicInteger count = new AtomicInteger(0);

    private int id;

    private double amount;

    private String startDate;

    private String endDate;

    private int companyId;

    private int userId;

    public Distribution() {

    }

    public Distribution(double amount, String startDate, String endDate, int companyId, int userId) {
        this.id = count.incrementAndGet();
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.companyId = companyId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Build the distributions.json file
     */
    public static void populateDistributions(List<Distribution> distributionList) {
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // convert book object to JSON file
        try {
            mapper.writeValue(Paths.get("distributions.json").toFile(), distributionList);
        } catch (IOException e) {
            LOGGER.error("Error while populing users.json at file creation");
            e.printStackTrace();
        }
    }
}
