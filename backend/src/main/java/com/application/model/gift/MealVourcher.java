package com.application.model.gift;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * MealVourcher card class.
 */
public class MealVourcher extends Advantage {

    private static final AtomicInteger count = new AtomicInteger(0);

    /**
     * Default constructor.
     */
    public MealVourcher() {
        super();
        this.id = count.incrementAndGet();
        this.cardValidity = LocalDate.now().withMonth(2).plusYears(1).with(lastDayOfMonth());
    }
    
}
