package com.application.model.gift;


import com.application.common.Consts;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Gift card class.
 */
public class GiftCard extends Advantage {

    private static final AtomicInteger count = new AtomicInteger(0);

    /**
     * Default constructor.
     */
    public GiftCard() {
        super();
        this.id = count.incrementAndGet();
        this.cardValidity = LocalDate.now().plusDays(Consts.CARD_MAX_VALIDITY);
    }

}
