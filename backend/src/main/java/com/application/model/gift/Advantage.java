package com.application.model.gift;

import java.time.LocalDate;

public abstract class Advantage {
    protected int id;
    protected double amount;
    protected LocalDate distributionDate;
    protected LocalDate cardValidity;

    protected Advantage() {
        this.amount = com.application.common.Consts.DEFAULT_AMOUNT;
        this.distributionDate = LocalDate.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(LocalDate distributionDate) {
        this.distributionDate = distributionDate;
    }

    public LocalDate getCardValidity() {
        return cardValidity;
    }

    public void setCardValidity(LocalDate cardValidity) {
        this.cardValidity = cardValidity;
    }
}
