package com.application.model;

import com.application.common.Consts;
import com.application.model.gift.GiftCard;
import com.application.model.gift.MealVourcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User class.
 */
public class User {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(User.class);

    private static final AtomicInteger count = new AtomicInteger(0);

    private int id;

    private int companyId;

    private double balance;

    private List<GiftCard> giftCardList;

    private List<MealVourcher> mealVourcherList;

    /**
     * Constructor.
     *
     * @param id
     * @param balance
     * @param companyId
     */
    public User(int id, double balance, int companyId) {
        this.id = id;
        this.balance = balance;
        this.companyId = companyId;
        this.giftCardList = new ArrayList<>();
        this.mealVourcherList = new ArrayList<>();
    }

    public User() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        balance = 0;
        for (GiftCard giftCard : giftCardList) {
            if (ChronoUnit.DAYS.between(giftCard.getDistributionDate(), LocalDate.now()) > (long) Consts.CARD_MAX_VALIDITY) {
                LOGGER.debug("The gift card : " + giftCard.getId() + " is expired.");
            } else {
                balance += giftCard.getAmount();
            }
        }
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public List<GiftCard> getGiftCardList() {
        return giftCardList;
    }

    public void setGiftCardList(List<GiftCard> giftCardList) {
        this.giftCardList = giftCardList;
    }

    public List<MealVourcher> getMealVourcherList() {
        return mealVourcherList;
    }

    public void setMealVourcherList(List<MealVourcher> mealVourcherList) {
        this.mealVourcherList = mealVourcherList;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * Build the users.json file
     */
    public static void populate() {
        try {
            Files.deleteIfExists(Paths.get("users.json"));
        } catch (IOException e) {
            LOGGER.error("Error while populing users.json at file deletion");
            e.printStackTrace();
        }
        // create users list
        List<User> userList = Arrays.asList(
                new User(count.incrementAndGet(), 100, 1),
                new User(count.incrementAndGet(), 0, 1),
                new User(count.incrementAndGet(), 0, 2)
        );
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // convert book object to JSON file
        try {
            mapper.writeValue(Paths.get("users.json").toFile(), userList);
        } catch (IOException e) {
            LOGGER.error("Error while populing users.json at file creation");
            e.printStackTrace();
        }
    }
}
