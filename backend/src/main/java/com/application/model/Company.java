package com.application.model;

import com.application.exception.CompanyNotFoundException;
import com.application.exception.NegativeBalanceException;
import com.application.model.gift.GiftCard;
import com.application.model.gift.MealVourcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Company class.
 */
public class Company {

    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(Company.class);

    private static final AtomicInteger count = new AtomicInteger(0);

    private int id;

    private String name;

    private double balance;

    private List<User> userList;

    private static final String COMPANY_JSON = "companies.json";

    public Company() {
    }

    public Company(int id, String name, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    /**
     * Method that allow to distribute gift cards to a the company's users.
     */
    public static synchronized List<Distribution> distributeGiftCard(int companyId) throws NegativeBalanceException, CompanyNotFoundException {
        Company company = extractCompany(companyId);
        if (company == null) {
            throw new CompanyNotFoundException("The company with id : " + companyId + " haven't been found");
        }
        extractUsers(company);

        List<Distribution> distributionList = new ArrayList<>();
        for (User user : company.getUserList()) {
            GiftCard giftCard = new GiftCard();
            if (company.balance - giftCard.getAmount() < 0) {
                String errorMessage = "The amount " + giftCard.getAmount() + " is superior to the remaining balance " + company.getBalance() + " for the company " + company.getName();
                LOGGER.debug(errorMessage);
                throw new NegativeBalanceException(errorMessage);
            }
            user.setBalance(user.getBalance() + giftCard.getAmount());
            company.setBalance(company.getBalance() - giftCard.getAmount());
            user.getGiftCardList().add(giftCard);
            distributionList.add(new Distribution(giftCard.getAmount(), giftCard.getDistributionDate().toString(), giftCard.getCardValidity().toString(), companyId, user.getId()));
        }
        Distribution.populateDistributions(distributionList);
        return distributionList;
    }

    /**
     * Method that allow to distribute gift cards to a the company's users.
     */
    public static synchronized List<Distribution> distributeMealVourcher(int companyId) throws NegativeBalanceException, CompanyNotFoundException {
        Company company = extractCompany(companyId);
        if (company == null) {
            throw new CompanyNotFoundException("The company with id : " + companyId + " haven't been found");
        }
        extractUsers(company);

        List<Distribution> distributionList = new ArrayList<>();
        for (User user : company.getUserList()) {
            MealVourcher mealVourcher = new MealVourcher();
            if (company.balance - mealVourcher.getAmount() < 0) {
                String errorMessage = "The amount " + mealVourcher.getAmount() + " is superior to the remaining balance " + company.getBalance() + " for the company " + company.getName();
                LOGGER.debug(errorMessage);
                throw new NegativeBalanceException(errorMessage);
            }
            user.setBalance(user.getBalance() + mealVourcher.getAmount());
            company.setBalance(company.getBalance() - mealVourcher.getAmount());
            user.getMealVourcherList().add(mealVourcher);
            distributionList.add(new Distribution(mealVourcher.getAmount(), mealVourcher.getDistributionDate().toString(), mealVourcher.getCardValidity().toString(), companyId, user.getId()));
        }
        Distribution.populateDistributions(distributionList);
        return distributionList;
    }

    /**
     * Extract JSON to list of users.
     *
     * @param company
     */
    private static void extractUsers(Company company) {
        List<User> userListFinal = new ArrayList<>();
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();
        // convert JSON string to List of users
        try {
            List<User> userList = Arrays.asList(mapper.readValue(Paths.get("users.json").toFile(), User[].class));
            for (User user : userList) {
                if (company.id == user.getCompanyId()) {
                    userListFinal.add(user);
                }
            }
            company.setUserList(userListFinal);
        } catch (IOException e) {
            LOGGER.error("Error While reading user.json content");
            e.printStackTrace();
        }
    }

    /**
     * Get the company by id.
     *
     * @param companyId
     * @return
     */
    private static Company extractCompany(int companyId) {
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();
        // convert JSON string to List of users
        List<Company> companyList = new ArrayList<>();
        try {
            companyList = Arrays.asList(mapper.readValue(Paths.get(COMPANY_JSON).toFile(), Company[].class));
            for (Company company : companyList) {
                if (company.getId() == companyId) {
                    return company;
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error While reading user.json content");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Build the company.json file
     */
    public static void populate() {
        try {
            Files.deleteIfExists(Paths.get(COMPANY_JSON));
        } catch (IOException e) {
            LOGGER.error("Error while populing companies.json at file deletion");
            e.printStackTrace();
        }
        // create users list
        List<Company> companyList = Arrays.asList(
                new Company(count.incrementAndGet(), "Wedoogift", 1000),
                new Company(count.incrementAndGet(), "Wedoofood", 3000)
        );
        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // convert company object to JSON file
        try {
            mapper.writeValue(Paths.get(COMPANY_JSON).toFile(), companyList);
        } catch (IOException e) {
            LOGGER.error("Error while populing companies.json at file creation");
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
