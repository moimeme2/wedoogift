/**
 * (C)Pharmagest Interactive<br/>
 * Cartouche Starteam :<br/>
 * $Revision$ $Date$ $Author$ $NoKeywords$
 */
package com.application.web;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Classe de configuration de Swagger
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    

    
}
