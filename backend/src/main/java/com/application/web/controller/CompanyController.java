package com.application.web.controller;

import com.application.exception.CompanyNotFoundException;
import com.application.exception.NegativeBalanceException;
import com.application.model.Company;
import com.application.model.Distribution;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @PostMapping(path = "distribute/gift/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<List<Distribution>> distributeGiftCard(@PathVariable(value = "id") int id) throws NegativeBalanceException, CompanyNotFoundException {
        return new ResponseEntity<>(Company.distributeGiftCard(id), HttpStatus.CREATED);
    }

    @PostMapping(path = "distribute/meal/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<List<Distribution>> distributemealVourcher(@PathVariable(value = "id") int id) throws NegativeBalanceException, CompanyNotFoundException {
        return new ResponseEntity<>(Company.distributeMealVourcher(id), HttpStatus.CREATED);
    }

}
