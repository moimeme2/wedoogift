package com.application;

import com.application.model.Company;
import com.application.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class Application {

    public static void main(String[] args) {

        Company.populate();
        User.populate();

        SpringApplication.run(Application.class, args);
    }
}
