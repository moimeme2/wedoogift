package com.application.common;

public class Consts {

    public static final double DEFAULT_AMOUNT = 100;

    public static final short CARD_MAX_VALIDITY = 365;

    private Consts() {
    }
}
