package com.application.exception;

/**
 * Class enabling the throwing of exception in case of gift card is expired (365 days).
 */
public class GiftCardExpiredException extends Exception {

    /**
     * Constructor.
     * @param message
     */
    public GiftCardExpiredException(String message) {
        super(message);
    }
}
