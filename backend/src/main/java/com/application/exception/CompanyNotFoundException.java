package com.application.exception;

/**
 * Class enabling the throwing of exception in case of company is not found.
 */
public class CompanyNotFoundException extends Exception {

    /**
     * Constructor.
     * @param message
     */
    public CompanyNotFoundException(String message) {
        super(message);
    }
}
