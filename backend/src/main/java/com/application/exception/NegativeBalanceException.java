package com.application.exception;

/**
 * Class enabling the throwing of exception in case of gift amount superior to the remaining company balance.
 */
public class NegativeBalanceException extends Exception {

    /**
     * Constructor.
     * @param message
     */
    public NegativeBalanceException(String message) {
        super(message);
    }
}
